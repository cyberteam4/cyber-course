import hashlib
import random
import string

def randomString(stringLength=10):
    """Generate groups random string of fixed length """
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(stringLength))




if __name__ == '__main__':
    str = randomString()
    result = hashlib.sha1(str.encode())
    #print(str)
    #print(result.hexdigest())

    letters = string.ascii_lowercase
    str = ''.join(random.choice(letters) for i in range(10))
    secret = hashlib.sha1(str.encode())
    #print(secret.hexdigest())
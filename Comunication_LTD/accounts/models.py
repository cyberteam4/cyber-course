from builtins import object

from django.contrib import auth
from django.contrib.auth.models import UserManager
from django.db import models
from django.utils import timezone


class User(auth.models.User, auth.models.PermissionsMixin):

    def __str__(self):
        return "@{}".format(self.username)
